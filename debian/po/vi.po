# Vietnamese translation for XawTV.
# Copyright © 2007 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2005-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: xawtv 3.95.dfsg.1-7\n"
"Report-Msgid-Bugs-To: xawtv@packages.debian.org\n"
"POT-Creation-Date: 2007-10-25 07:29+0200\n"
"PO-Revision-Date: 2007-10-25 22:47+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.7b1\n"

#. Type: boolean
#. Description
#: ../xawtv.templates:2001
msgid "Create video4linux (/dev/video*) special files?"
msgstr "Có tạo tập tin video4linux (/dev/video*) đặc biệt không?"

#. Type: boolean
#. Description
#: ../xawtv.templates:3001
msgid "Scan for TV stations?"
msgstr "Có quét tìm các kênh TV không?"

#. Type: boolean
#. Description
#: ../xawtv.templates:3001
msgid ""
"A list of TV stations found by scanning can be included in the configuration "
"file."
msgstr "Tìm được thì danh sách các kênh TV nằm trong tập tin cấu hình."

#. Type: boolean
#. Description
#: ../xawtv.templates:3001
msgid ""
"This requires a working bttv driver. If bttv isn't configured correctly, TV "
"stations will not be found."
msgstr ""
"Việc này cần thiết trình điều khiển bttv hoạt động. Chưa cấu hình đúng bttv "
"thì không tìm thấy kênh TV."

#. Type: boolean
#. Description
#: ../xawtv.templates:3001
msgid ""
"Channel names will be retrieved from teletext information, which will only "
"work for PAL channels."
msgstr ""
"Lấy các tên kênh từ thông tin teletext: chỉ hoạt động cho kệnh kiểu PAL."

#. Type: select
#. Description
#: ../xawtv.templates:4001
msgid "TV standard:"
msgstr "Tiêu chuẩn TV:"

#. Type: boolean
#. Description
#: ../xawtv.templates:5001
msgid "Create a default configuration for xawtv?"
msgstr "Có tạo cấu hình mặc định cho xawtv không?"

#. Type: boolean
#. Description
#: ../xawtv.templates:5001
msgid ""
"A system-wide configuration file for xawtv can be created with reasonable "
"default values for the local country."
msgstr ""
"Có thể tạo cho xawtv một tập tin cấu hình toàn hệ thống chứa các giá trị hơi "
"thích hợp với quốc gia đó."

#. Type: boolean
#. Description
#: ../xawtv.templates:5001
msgid ""
"That file is not required but will simplify software configuration for users."
msgstr ""
"Tập tin này không phải bắt buộc phải dùng, nhưng vẫn còn đơn giản hoá tiến "
"trình cấu hình phần mềm cho người dùng."

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "us-bcast"
msgstr "Mỹ-phát_thanh"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "us-cable"
msgstr "Mỹ-cáp"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "us-cable-hrc"
msgstr "Mỹ-cáp-hrc"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "japan-bcast"
msgstr "Nhật-phát_thanh"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "japan-cable"
msgstr "Nhật-cáp"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "europe-west"
msgstr "Tây-Âu"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "europe-east"
msgstr "Đông-Âu"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "italy"
msgstr "Ý"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "newzealand"
msgstr "Niu-Xi-Lan"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "australia"
msgstr "Úc"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "ireland"
msgstr "Ai-len"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "france"
msgstr "Pháp"

#. Type: select
#. Choices
#: ../xawtv.templates:6001
msgid "china-bcast"
msgstr "Trung-phát_thanh"

#. Type: select
#. Description
#: ../xawtv.templates:6002
msgid "Frequency table to use:"
msgstr "Bảng tần số cần dùng:"

#. Type: select
#. Description
#: ../xawtv.templates:6002
msgid ""
"A frequency table is a list of TV channel names and numbers with their "
"broadcast frequencies."
msgstr ""
"Bảng tần số là danh sách các tên/số kênh TV và tần số phát hành tương ứng."

#~ msgid ""
#~ "This can do a scan of all channels and put a list of the TV stations I've "
#~ "found into the config file."
#~ msgstr ""
#~ "Trình này có thể quét mọi kênh rồi ghi vào tập tin cấu hình một danh sách "
#~ "các kênh TV đã tìm."

#~ msgid ""
#~ "I'll try to pick up the channel names from videotext. This will work with "
#~ "PAL only."
#~ msgstr ""
#~ "Trình này sẽ cố lấy tên kênh từ videotext (chữ ảnh động). Sẽ hoạt động "
#~ "chỉ với giao thức PAL."

#~ msgid "PAL, SECAM, NTSC"
#~ msgstr "PAL, SECAM, NTSC"

#~ msgid "TV norm is used in your country:"
#~ msgstr "Tiêu chuẩn TV dùng ở chỗ bạn:"

#~ msgid ""
#~ "It is not required to have a global configuration file, but it will be "
#~ "more comfortable for your users if they find a working default "
#~ "configuration."
#~ msgstr ""
#~ "Không cần tập tin cấu hình toàn cục, nhưng nó tiện hơn cho người dùng nếu "
#~ "họ tìm cấu hình hoạt động."

#~ msgid ""
#~ "us-bcast, us-cable, us-cable-hrc, japan-bcast, japan-cable, europe-west, "
#~ "europe-east, italy, newzealand, australia, ireland, france, china-bcast"
#~ msgstr ""
#~ "phát_thanh_Mỹ, cáp_Mỹ, cáp_Mỹ_hrc, phát_thanh_Nhật_bản, cáp_Nhật_bản, "
#~ "Tây_Âu, Đông_Âu, Ý, Niu_Di-lan, Úc, Ái-nhĩ-lan, Pháp, "
#~ "phát_thanh_Quốc_Trung"
